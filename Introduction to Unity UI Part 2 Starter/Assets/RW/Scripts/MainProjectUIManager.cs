﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainProjectUIManager : MonoBehaviour
{
    public Animator orisitButton;
    public Animator helpquestionPanel;
    public Animator notherePanel;

    public void OrIsItOpen()
    {
        bool isHidden = orisitButton.GetBool("isHidden");
        orisitButton.SetBool("isHidden", true);
    }
  
    public void HelpQuestionOpen()
    {
        bool isHidden = helpquestionPanel.GetBool("isHidden");
        helpquestionPanel.SetBool("isHidden", true);

    }

    public void NotHerePanelDown()
    {
        int isHidden = notherePanel.GetInteger("isHidden");
        notherePanel.SetInteger("isHidden", 1);
    }

}
